
# Install

```sh
1. conda install scikit-learn
2. git clone https://gitlab.com/vinutah/gauss-ml.git
3. cd gauss-ml/implementation/
4. jupyter notebook Hands\ On\ ML\ \(scikit\).ipynb
```

# Coding Language

Scikit-learn is largely written in Python.

Some core algorithms written in Cython to achieve performance. 

# Alogorithms

Support vector machines are implemented 
by a Cython wrapper around ```LIBSVM```.

Logistic regression and Linear support vector machines 
by a similar wrapper around ```LIBLINEAR```.

# Origin and Current State

* The scikit-learn project started as scikits.learn, 
	* A Google Summer of Code project by David Cournapeau. 
	* Its name stems from the notion that it is a "SciKit" (SciPy Toolkit), 
	* Was meant to be a separately-developed and distributed third-party extension to SciPy.
	* The original codebase was later rewritten by other developers. 

* Dates
	* 2010 INRIA took leadership of the project and made the first public release 
	* 2012 Of the various scikits, scikit-learn as well as scikit-image were described as "well-maintained and popular".
	* 2017 scikit-learn is under active development.

# Python Libraries

## PREPROCESSING WITH PANDAS

	* Reading data
	* Selecting columns and rows
	* Filtering
	* Vectorized string operations
	* Missing values
	* Handling time
	* Time series


## NUMPY, SCIPY

	* Arrays
	* Indexing, Slicing, and Iterating
	* Reshaping
	* Shallow vs deep copy
	* Broadcasting
	* Indexing (advanced)
	* Matrices
	* Matrix decompositions


## SCIKIT-LEARN

	* Feature extraction
	* Classification
	* Regression
	* Clustering
	* Dimension reduction
	* Model selection
